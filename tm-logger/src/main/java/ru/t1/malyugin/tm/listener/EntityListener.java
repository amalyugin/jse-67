package ru.t1.malyugin.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.ILoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class EntityListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService;

    @Autowired
    public EntityListener(@NotNull final ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String text = textMessage.getText();
        loggerService.log(text);
    }

}