package ru.t1.malyugin.tm.configuration;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import ru.t1.malyugin.tm.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.malyugin.tm")
public class LoggerConfiguration {

    @NotNull
    private final IPropertyService propertyService;

    @Autowired
    public LoggerConfiguration(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Bean
    @NotNull
    public ConnectionFactory jmsConnectionFactory() {
        @NotNull final String url = propertyService.getQueueUrl();
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        return connectionFactory;
    }

    @Bean
    public MongoClient mongoClient() {
        @NotNull String host = propertyService.getMongoHost();
        @NotNull String port = propertyService.getMongoPort();
        @NotNull String connectionString = "mongodb://" + host + ":" + port;
        return MongoClients.create(connectionString);
    }

    @Bean
    public MongoTemplate mongoTemplate(@NotNull final MongoClient mongoClient) {
        @NotNull final String mongoDatabase = propertyService.getMongoDBName();
        return new MongoTemplate(mongoClient, mongoDatabase);
    }

}