package ru.t1.malyugin.tm.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.client.ProjectRestEndpointClient;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.IntegrationCategory;
import ru.t1.malyugin.tm.model.Project;

@Category(IntegrationCategory.class)
public class ProjectTest {

    private static final ProjectRestEndpointClient CLIENT = ProjectRestEndpointClient.getInstance();
    private final Project project1 = new Project("P1", "D1");
    private final Project project2 = new Project("P2", "D2");

    @Before
    public void before() {
        CLIENT.post(project1);
        CLIENT.post(project2);
    }

    @After
    public void after() {
        CLIENT.delete(project1.getId());
        CLIENT.delete(project2.getId());
    }


    @Test
    public void getByIdTest() {
        Assert.assertNotNull(CLIENT.get(project1.getId()));
    }

    @Test
    public void addTest() {
        final Project project = new Project("NEW", "NEW");
        CLIENT.post(project);
        Assert.assertNotNull(CLIENT.get(project.getId()));
        CLIENT.delete(project.getId());
    }

    @Test
    public void deleteByIdTest() {
        CLIENT.delete(project1.getId());
        Assert.assertNull(CLIENT.get(project1.getId()));
        Assert.assertNotNull(CLIENT.get(project2.getId()));
    }

    @Test
    public void editTest() {
        Assert.assertNotNull(CLIENT.get(project1.getId()));
        project1.setStatus(Status.IN_PROGRESS);
        project1.setDescription("NEW");
        CLIENT.put(project1);
        final Project updatedProject = CLIENT.get(project1.getId());
        Assert.assertEquals(project1.getId(), updatedProject.getId());
        Assert.assertEquals(project1.getStatus(), updatedProject.getStatus());
        Assert.assertEquals(project1.getDescription(), updatedProject.getDescription());
    }

}