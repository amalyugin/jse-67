package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/projects")
public interface ProjectsEndpoint {

    @WebMethod
    @GetMapping("/get")
    Collection<Project> get();

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody Collection<Project> projects
    );

    @WebMethod
    @DeleteMapping("/delete/all")
    void clear();

}