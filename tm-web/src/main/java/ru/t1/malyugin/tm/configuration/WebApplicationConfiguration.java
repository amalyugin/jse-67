package ru.t1.malyugin.tm.configuration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.t1.malyugin.tm.api.endpoint.ProjectEndpoint;
import ru.t1.malyugin.tm.api.endpoint.ProjectsEndpoint;
import ru.t1.malyugin.tm.api.endpoint.TaskEndpoint;
import ru.t1.malyugin.tm.api.endpoint.TasksEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@ComponentScan({"ru.t1.malyugin.tm.controller", "ru.t1.malyugin.tm.endpoint"})
public class WebApplicationConfiguration implements WebApplicationInitializer {

    @Bean
    public ViewResolver internalResourceViewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint, SpringBus springBus) {
        final EndpointImpl endpoint = new EndpointImpl(springBus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectsEndpointRegistry(final ProjectsEndpoint projectsEndpoint, SpringBus springBus) {
        final EndpointImpl endpoint = new EndpointImpl(springBus, projectsEndpoint);
        endpoint.publish("/ProjectsEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint, SpringBus springBus) {
        final EndpointImpl endpoint = new EndpointImpl(springBus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint tasksEndpointRegistry(final TasksEndpoint tasksEndpoint, SpringBus springBus) {
        final EndpointImpl endpoint = new EndpointImpl(springBus, tasksEndpoint);
        endpoint.publish("/TasksEndpoint");
        return endpoint;
    }

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}