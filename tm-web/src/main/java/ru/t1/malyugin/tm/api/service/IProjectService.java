package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;

public interface IProjectService {

    Collection<Project> findAll();

    long count();

    void create();

    void add(Project project);

    void deleteById(String id);

    void deleteAll(Collection<Project> projects);

    void clear();

    void edit(Project project);

    Project findById(String id);

}