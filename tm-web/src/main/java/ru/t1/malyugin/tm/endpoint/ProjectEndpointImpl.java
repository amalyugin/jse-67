package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.ProjectEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/get/{id}")
    public Project get(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/post")
    public void post(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    ) {
        projectService.add(project);
    }

    @Override
    @WebMethod
    @PutMapping("/put")
    public void put(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    ) {
        projectService.edit(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        projectService.deleteById(id);
    }

}