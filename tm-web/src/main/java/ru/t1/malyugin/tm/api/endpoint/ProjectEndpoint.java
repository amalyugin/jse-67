package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/project")
public interface ProjectEndpoint {

    @WebMethod
    @GetMapping("/get/{id}")
    Project get(
            @WebParam(name = "id", partName = "id")
            @PathVariable String id
    );

    @WebMethod
    @PostMapping("/post")
    void post(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PutMapping("/put")
    void put(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable String id
    );

}