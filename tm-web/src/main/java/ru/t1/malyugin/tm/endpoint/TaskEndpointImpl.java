package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.TaskEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/get/{id}")
    public Task get(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/post")
    public void post(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.add(task);
    }

    @Override
    @WebMethod
    @PutMapping("/put")
    public void put(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.edit(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
    }

}