package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/task")
public interface TaskEndpoint {

    @WebMethod
    @GetMapping("/get/{id}")
    Task get(
            @WebParam(name = "id", partName = "id")
            @PathVariable String id
    );

    @WebMethod
    @PostMapping("/post")
    void post(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PutMapping("/put")
    void put(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable String id
    );

}