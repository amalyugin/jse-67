package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.TasksEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.TasksEndpoint")
public class TasksEndpointImpl implements TasksEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/get")
    public Collection<Task> get() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public int count() {
        return (int) taskService.count();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody Collection<Task> tasks
    ) {
        taskService.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/all")
    public void clear() {
        taskService.clear();
    }


}