package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface TasksEndpoint {

    @WebMethod
    @GetMapping("/get")
    Collection<Task> get();

    @WebMethod
    @GetMapping("/count")
    int count();

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody Collection<Task> tasks
    );

    @WebMethod
    @DeleteMapping("/delete/all")
    void clear();

}