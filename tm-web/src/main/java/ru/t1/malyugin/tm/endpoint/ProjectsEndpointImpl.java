package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.ProjectsEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ProjectsEndpoint")
public class ProjectsEndpointImpl implements ProjectsEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/get")
    public Collection<Project> get() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return (int) projectService.count();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody Collection<Project> projects
    ) {
        projectService.deleteAll(projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/all")
    public void clear() {
        projectService.clear();
    }

}