<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<form:form action="/task/edit/${task.id}" method="post" modelAttribute="task">
    <h1>TASK CREATE</h1>
    <fieldset>

    <form:input path="id" type="hidden"/>

    <form:label path="name">Name:</form:label>
    <form:input path="name" type="text"/>

    <form:label path="description">Description:</form:label>
    <form:input path="description" type="text"/>

    <form:label path="status">Status:</form:label>
    <form:select path="status">
        <form:option value="${null}" label="--- // ---"/>
        <form:options items="${statuses}" itemLabel="displayName"/>
    </form:select>

    <form:label path="projectId">Project:</form:label>
    <form:select path="projectId">
        <form:option value="${null}" label="--- // ---"/>
        <form:options items="${projects}" itemLabel="name" itemValue="id"/>
    </form:select>

    <form:label path="start">Start Date:</form:label>
    <form:input path="start" type="date"/>

    <form:label path="finish">Finish Date:</form:label>
    <form:input path="finish" type="date"/>

    <fieldset>

    <button type="reset">RESET</button>
    <button type="submit">SAVE TASK</button>
</form:form>
<br/>
<jsp:include page="../include/_footer.jsp"/>