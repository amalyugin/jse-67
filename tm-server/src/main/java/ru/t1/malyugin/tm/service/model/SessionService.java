package ru.t1.malyugin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.model.ISessionService;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.repository.model.SessionRepository;

@Service
public class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    private final SessionRepository sessionRepository;

    @Autowired
    public SessionService(@NotNull final SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @NotNull
    @Override
    protected SessionRepository getRepository() {
        return sessionRepository;
    }

}