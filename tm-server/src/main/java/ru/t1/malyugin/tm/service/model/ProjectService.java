package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.model.IProjectService;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.model.ProjectRepository;

@Service
public class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final IUserService userService;

    @Autowired
    public ProjectService(
            @NotNull final ProjectRepository projectRepository,
            @NotNull final IUserService userService
    ) {
        this.projectRepository = projectRepository;
        this.userService = userService;
    }

    @NotNull
    @Override
    protected ProjectRepository getRepository() {
        return projectRepository;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        update(project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        @NotNull final Project project = new Project();
        project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        if (user == null) throw new UserNotFoundException();
        project.setUser(user);
        add(project);
        return project;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final User user = userService.findOneById(userId.trim());
        if (user == null) throw new UserNotFoundException();
        return getRepository().existsByUserAndId(user, id.trim());
    }

}