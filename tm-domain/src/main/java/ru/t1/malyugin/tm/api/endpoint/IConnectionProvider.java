package ru.t1.malyugin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}