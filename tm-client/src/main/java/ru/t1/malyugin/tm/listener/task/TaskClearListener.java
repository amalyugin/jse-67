package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.task.TaskClearRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-clear";

    @NotNull
    private static final String DESCRIPTION = "Clear all tasks";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        taskEndpoint.clearTask(request);
    }

}