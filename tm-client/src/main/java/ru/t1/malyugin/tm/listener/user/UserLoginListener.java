package ru.t1.malyugin.tm.listener.user;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-login";

    @NotNull
    private static final String DESCRIPTION = "user login";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

    @Override
    @EventListener(condition = "@userLoginListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull ConsoleEvent event) {
        System.out.println("[LOGIN]");

        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @Nullable final String token = authEndpoint.login(request).getToken();
        setToken(token);
    }

}