package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-complete-by-id";

    @NotNull
    private static final String DESCRIPTION = "Complete task by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[COMPLETE TASK BY ID]");

        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken(), id);
        taskEndpoint.completeTaskById(request);
    }

}